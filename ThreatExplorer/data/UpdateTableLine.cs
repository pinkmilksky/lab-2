﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreatExplorer
{
    public class UpdateTableLine
    {
        public string ThreatId { get; set; }
        public string Status { get; set; }             

        public Threat Threat { get; set; }
    }
}
