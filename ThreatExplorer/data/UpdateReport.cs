﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreatExplorer
{
    public class UpdateReport
    {
        public bool IsFailed { get; set; } = false;
        public string FailReason { get; set; } = "";
        public Dictionary<Threat, UpdateState> IdUpdateStates { get; set; } = new Dictionary<Threat, UpdateState>();

        // threat before update - threat after update
        public Dictionary<Threat, Threat> ChangedRecords { get; set; } = new Dictionary<Threat, Threat>();
        
    }
}
