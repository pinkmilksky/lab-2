﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreatExplorer
{
    public enum UpdateState
    {
        Changed,
        Deleted,
        Added,
        NotChanged
    }
}
