﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreatExplorer
{

    public class Threat
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public string Object { get; set; }
        public bool Confidentiality { get; set; }
        public bool Integrity { get; set; }
        public bool Accessibility { get; set; }
        public DateTime FoundDate { get; set; }
        public DateTime ChangeDate { get; set; }

        public static bool operator ==(Threat a, Threat b)
        {
            if (a.Id != b.Id) return false;
            if (a.Name != b.Name) return false;
            if (a.Description != b.Description) return false;
            if (a.Source != b.Source) return false;
            if (a.Object != b.Object) return false;
            if (a.Confidentiality != b.Confidentiality) return false;
            if (a.Accessibility != b.Accessibility) return false;
            if (a.FoundDate != b.FoundDate) return false;
            if (a.ChangeDate != b.ChangeDate) return false;
            return true;
        }

        public static bool operator !=(Threat a, Threat b)
        {
            return !(a == b);
        }

        public override bool Equals(Object obj)
        {
            Threat threat = (Threat)obj;
            return this == threat;
        }

        public override int GetHashCode()
        {
            return this.Id.GetHashCode();
        }

        public override string ToString()
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(Id);
            sb.Append("\n");

            //sb.Append("Наименование угрозы:\n");
            sb.Append(Name);
            sb.Append("\n\n");

            //sb.Append("Описание угрозы:\n");
            sb.Append(Description);
            sb.Append("\n\n");

            sb.Append("Источник угрозы: ");
            sb.Append(Source);
            sb.Append("\n\n");

            sb.Append("Объект угрозы: ");
            sb.Append(Object);
            sb.Append("\n\n");

            sb.Append("Нарушение конфиденциальности: ");
            sb.Append(Confidentiality == true ? "да" : "нет");
            sb.Append("\n");
            sb.Append("Нарушение целостности: ");
            sb.Append(Integrity == true ? "да" : "нет");
            sb.Append("\n");
            sb.Append("Нарушение доступности: ");
            sb.Append(Accessibility == true ? "да" : "нет");
            sb.Append("\n\n");

            sb.Append("Обнаружена ");
            sb.Append(FoundDate.ToString("dd.MM.yyyy"));
            sb.Append("\nПоследнее изменение ");
            sb.Append(ChangeDate.ToString("dd.MM.yyyy"));

            return sb.ToString();
        }

    }


}
