﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace ThreatExplorer
{
    class FileDownloader
    {
        private const string fileLink = "https://bdu.fstec.ru/documents/files/thrlist.xlsx";
        private const string fileName = "thrlist.xlsx";
        private static WebClient webClient = new WebClient();
        public static FileInfo DownloadFile()
        {
            webClient.DownloadFile(fileLink, fileName);
            return new FileInfo(fileName);
        }
    }
}
