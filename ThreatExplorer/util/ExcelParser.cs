﻿using OfficeOpenXml;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ThreatExplorer
{
    class ExcelParser : IDisposable
    {
        private const int firstRow = 3;
        private ExcelPackage excelPackage;
        private ExcelWorksheet worksheet;

        public ExcelParser(FileInfo table)
        {
            excelPackage = new ExcelPackage(table);
        }

        public void Dispose()
        {
            excelPackage.Dispose();
        }

        public List<Threat> ParseTable()
        {
            worksheet = excelPackage.Workbook.Worksheets[1];
            List<Threat> threats = new List<Threat>();
            for (int i = firstRow; i <= worksheet.Dimension.End.Row; i++)
            {
                Threat threat = ParseRow(i);
                threats.Add(threat);
            }
            
            return threats;
        }

        private Threat ParseRow(int rowNumber)
        {
            Threat threat = new Threat();
            threat.Id = "УБИ." + Int32.Parse(worksheet.Cells[rowNumber, 1].Text).ToString("D3");
            threat.Name = worksheet.Cells[rowNumber, 2].Text;
            threat.Description = worksheet.Cells[rowNumber, 3].Text;
            threat.Source = worksheet.Cells[rowNumber, 4].Text;
            threat.Object = worksheet.Cells[rowNumber, 5].Text;
            threat.Confidentiality = worksheet.Cells[rowNumber, 6].Text == "1";
            threat.Integrity = worksheet.Cells[rowNumber, 7].Text == "1";
            threat.Accessibility = worksheet.Cells[rowNumber, 8].Text == "1";
            threat.FoundDate = DateTime.Parse(worksheet.Cells[rowNumber, 9].Text);
            threat.ChangeDate = DateTime.Parse(worksheet.Cells[rowNumber, 9].Text);

            return threat;
        }

    }
}
