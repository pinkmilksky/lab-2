﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ThreatExplorer
{
    class FileIO
    {
        public static void SerializeAndSave(string path, List<Threat> threats)
        {
            var serializer = new XmlSerializer(typeof(List<Threat>));
            using (var writer = new StreamWriter(path, false, Encoding.UTF8))
            {
                serializer.Serialize(writer, threats);
            }
        }

        public static List<Threat> ReadAndDeserialize(string path)
        {
            var serializer = new XmlSerializer(typeof(List<Threat>));
            using (var reader = new StreamReader(path))
            {
                return (List<Threat>)serializer.Deserialize(reader);
            }
        }
    }
}