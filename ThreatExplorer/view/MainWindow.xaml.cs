﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;


namespace ThreatExplorer
{

    public partial class MainWindow : Window
    {
        static ThreatStorage threatStorage = new ThreatStorage();
        List<Threat> displayedThreats = new List<Threat>();
        private int currentPageNumber = 0;
        private const int pageSize = 15;

        public MainWindow()
        {
            InitializeComponent();
            try
            {
                threatStorage.Load();
            }
            catch (Exception)
            {
                MessageBox.Show("Локальная копия не найдена, будет выполнено полное обновление");
                Update();
            }
            displayedThreats = GetPage(currentPageNumber);
            gridForThreats.ItemsSource = displayedThreats;
            prevButton.IsEnabled = false;
        }

        private void prevButton_Click(object sender, RoutedEventArgs e)
        { 
            displayedThreats = GetPage(--currentPageNumber);
            gridForThreats.ItemsSource = null;
            gridForThreats.ItemsSource = displayedThreats;
        }

        private void nextButton_Click(object sender, RoutedEventArgs e)
        {
            displayedThreats = GetPage(++currentPageNumber);
            gridForThreats.ItemsSource = null;
            gridForThreats.ItemsSource = displayedThreats;
        }

        private List<Threat> GetPage(int pageNumber)
        {
            int pageStart = 15 * pageNumber;
            int thisPageSize = 15;
            prevButton.IsEnabled = (pageNumber != 0);
            nextButton.IsEnabled = (pageNumber != threatStorage.Threats.Count / pageSize);
            if (threatStorage.Threats.Count - 15 * pageNumber < pageSize)
            {
                thisPageSize = threatStorage.Threats.Count % 15;
            }
            return threatStorage.Threats.GetRange(pageStart, thisPageSize);
        }

        private void DataGridRow_MouseDown(object sender, MouseButtonEventArgs e)
        {
            Threat selected = (Threat)gridForThreats.SelectedItem;

            MessageBox.Show(selected.ToString());
        }

        private void updateButton_Click(object sender, RoutedEventArgs e)
        {
            Update();
        }

        private void saveButton_Click(object sender, RoutedEventArgs e)
        {
            threatStorage.Save();
            MessageBox.Show("Saved");

        }

        private void loadButton_Click(object sender, RoutedEventArgs e)
        {
            currentPageNumber = 0;
            threatStorage.Load();
            displayedThreats = GetPage(currentPageNumber);
            gridForThreats.ItemsSource = null;
            gridForThreats.ItemsSource = displayedThreats;
            MessageBox.Show("Loaded " + threatStorage.Threats.Count + " elements");

        }

        private void Update()
        {
            currentPageNumber = 0;
            UpdateReportWindow reportWindow = new UpdateReportWindow();
            reportWindow.SetReport(threatStorage.FullUpdate());
            reportWindow.Show();
            displayedThreats = GetPage(currentPageNumber);
            gridForThreats.ItemsSource = null;
            gridForThreats.ItemsSource = displayedThreats;
        }
    }
}
