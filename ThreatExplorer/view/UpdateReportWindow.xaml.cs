﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ThreatExplorer
{
    /// <summary>
    /// Логика взаимодействия для UpdateReportWindow.xaml
    /// </summary>
    public partial class UpdateReportWindow : Window
    {
        private List<UpdateTableLine> lines = new List<UpdateTableLine>();
        private UpdateReport currentReport;

        public UpdateReportWindow()
        {
            InitializeComponent();
        }

        public void SetReport(UpdateReport report)
        {
            currentReport = report;
            foreach (var state in report.IdUpdateStates)
            {
                if (state.Value == UpdateState.NotChanged) continue;
                UpdateTableLine line = new UpdateTableLine
                {
                    ThreatId = state.Key.Id,
                    Status = state.Value.ToString(),
                    Threat = state.Key
                };
                lines.Add(line);
            }
            table.ItemsSource = lines;
        }

        private void DataGridRow_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            UpdateTableLine selected = (UpdateTableLine) table.SelectedItem;
            if (currentReport.ChangedRecords.ContainsKey(selected.Threat))
            {
                before.Text = selected.Threat.ToString();
                after.Text = currentReport.ChangedRecords[selected.Threat].ToString();
            }
            else if (selected.Status == UpdateState.Deleted.ToString())
            {
                before.Text = selected.Threat.ToString();
                after.Text = "";
            }
            else
            {
                after.Text = selected.Threat.ToString();
                before.Text = "";
            }
        }
    }
}
