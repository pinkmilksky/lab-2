﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;


namespace ThreatExplorer
{
    class ThreatStorage
    {
        public List<Threat> Threats { get; private set; } = new List<Threat>();
        private const string path = "kek.xml";

        public UpdateReport FullUpdate()
        {
            UpdateReport report = new UpdateReport();
            try
            {
                FileInfo file = FileDownloader.DownloadFile();

                using (ExcelParser excelParser = new ExcelParser(file))
                {
                    List<Threat> threats = excelParser.ParseTable();
                    report = Compare(threats);
                    Threats = excelParser.ParseTable();
                }
            }
            catch (System.Net.WebException)
            {
                report.IsFailed = true; 
                report.FailReason = "Ошибка при скачивании файла";
            }

            return report;
        }

        public void Save()
        {
            FileIO.SerializeAndSave(path, Threats);
        }

        public void Load()
        {
            Threats = FileIO.ReadAndDeserialize(path);

        }

        private UpdateReport Compare(List<Threat> newList)
        {
            UpdateReport report = new UpdateReport();
            report.ChangedRecords = new Dictionary<Threat, Threat>();
            report.IdUpdateStates = new Dictionary<Threat, UpdateState>();

            ISet<Threat> newThreatSet = new HashSet<Threat>(newList);
            foreach (Threat threat in Threats)
            {
                try
                {
                    Threat newThreat = GetOneById(threat.Id, newThreatSet);
                    if (newThreat == threat)
                    {
                        report.IdUpdateStates.Add(threat, UpdateState.NotChanged);
                    }
                    else
                    {
                        report.IdUpdateStates.Add(threat, UpdateState.Changed);
                        report.ChangedRecords.Add(threat, newThreat);
                    }

                    bool f = newThreat.Equals(newThreat);
                    newThreatSet.Remove(newThreat);
                }
                catch (KeyNotFoundException)
                {
                    report.IdUpdateStates.Add(threat, UpdateState.Deleted);
                }
            }
            // now process added threats
            foreach (Threat threat in newThreatSet)
            {
                report.IdUpdateStates.Add(threat, UpdateState.Added);
            }

            return report;
        }

        public Threat GetOneById(String id, ISet<Threat> threatList)
        {
            foreach (Threat threat in threatList)
            {
                if (threat.Id == id) return threat;
            }
            throw new KeyNotFoundException();
        }
    }
}
